-- Intllib.
local S = mypaint_default.gettext

local mypaint_dirt_colors = mypaint_default.colors

local paintables = {
	"default:dirt",
	"default:dirt_with_grass",
	"default:dirt_with_dry_grass",
	"default:dirt_with_snow"
}

for _, entry in ipairs(mypaint_dirt_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"

-- Dirt
local block_id = "default:dirt"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_dirt.png".. paint},
	groups = {crumbly = 3, soil = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults(),
})

-- Dirt with Grass
local block_id = "default:dirt_with_grass"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_grass.png"..paint, "default_dirt.png"..paint,
		{name = "default_dirt.png^default_grass_side.png"..paint,
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults(),
})

-- Dirt with Dry Grass
local block_id = "default:dirt_with_dry_grass"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_dry_grass.png"..paint, "default_dirt.png"..paint,
		{name = "default_dirt.png^default_dry_grass_side.png"..paint,
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults(),
})

-- Dirt with Snow
local block_id = "default:dirt_with_snow"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_snow.png"..paint, "default_dirt.png"..paint,
		{name = "default_dirt.png^default_snow_side.png"..paint,
			tileable_vertical = false}},
	groups = {crumbly = 3, soil = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults(),
})


end

local colors = {}
for _, entry in ipairs(mypaint_dirt_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

