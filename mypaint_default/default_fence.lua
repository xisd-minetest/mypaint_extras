-- Intllib.
local S = mypaint_default.gettext

local mypaint_fence_colors = mypaint_default.colors

local paintables = {
	"default:fence_wood","default:fence_acacia_wood","default:fence_junglewood","default:fence_aspen_wood","default:fence_pine_wood",
	--"doors:gate_wood_closed","doors:gate_wood_open","doors:gate_wood",
	--"doors:gate_pine_wood_closed","doors:gate_pine_wood_open","doors:gate_pine_wood",
	--"doors:gate_acacia_wood_closed","doors:gate_acacia_wood_open","doors:gate_acacia_wood",
	--"doors:gate_junglewood_closed","doors:gate_junglewood_open","doors:gate_junglewood",
	--"doors:gate_aspen_wood_closed","doors:gate_aspen_wood_open","doors:gate_aspen_wood",
}

for _, entry in ipairs(mypaint_fence_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"


-- Wood Fence
local block_id = "default:fence_wood"
default.register_fence(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	texture = "default_fence_wood.png".. paint,
	material = "default:fence_wood_" .. color,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})
--[[
if minetest.get_modpath("doors") then
	local block_id = "doors:gate_wood"
	doors.register_fencegate(':' .. block_id .. '_' .. color, {
		description =  minetest.registered_nodes[block_id.."_closed"].description,
		texture = "default_wood.png".. paint,
		material = "default:fence_wood_" .. color,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		sounds = default.node_sound_wood_defaults(),
	})
end
--]]
-- Acacia Fence
local block_id = "default:fence_acacia_wood"
default.register_fence(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	texture = "default_fence_acacia_wood.png".. paint,
	material = "default:fence_acacia_wood_" .. color,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})
--[[
if minetest.get_modpath("doors") then
local block_id = "doors:gate_acacia_wood"
	doors.register_fencegate(':' .. block_id .. '_' .. color, {
		description =  minetest.registered_nodes[block_id.."_closed"].description,
		texture = "default_acacia_wood.png".. paint,
		material = "default:fence_acacia_wood_" .. color,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		sounds = default.node_sound_wood_defaults(),
	})
end
--]]
-- Jungle Fence
local block_id = "default:fence_junglewood"
default.register_fence(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	texture = "default_fence_junglewood.png".. paint,
	material = "default:fence_junglewood_" .. color,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})
--[[
if minetest.get_modpath("doors") then
	local block_id = "doors:gate_junglewood"
	doors.register_fencegate(':' .. block_id .. '_' .. color, {
		description =  minetest.registered_nodes[block_id.."_closed"].description,
		texture = "default_junglewood.png".. paint,
		material = "default:fence_junglewood_" .. color,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		sounds = default.node_sound_wood_defaults(),
	})
end
--]]
-- Aspen Fence
local block_id = "default:fence_aspen_wood"
default.register_fence(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	texture = "default_fence_aspen_wood.png".. paint,
	material = "default:fence_aspen_wood_" .. color,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})
--[[
if minetest.get_modpath("doors") then
	local block_id = "doors:gate_aspen_wood"
	doors.register_fencegate(':' .. block_id .. '_' .. color, {
		description =  minetest.registered_nodes[block_id.."_closed"].description,
		texture = "default_aspen_wood.png".. paint,
		material = "default:fence_aspen_wood_" .. color,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		sounds = default.node_sound_wood_defaults(),
	})
end
--]]
-- Pine Fence
local block_id = "default:fence_pine_wood"
default.register_fence(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	texture = "default_fence_pine_wood.png".. paint,
	material = "default:fence_pine_wood_" .. color,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})
--[[
if minetest.get_modpath("doors") then
	local block_id = "doors:gate_pine_wood"
	doors.register_fencegate(':' .. block_id .. '_' .. color, {
		description =  minetest.registered_nodes[block_id.."_closed"].description,
		texture = "default_pine_wood.png".. paint,
		material = "default:fence_pine_wood_" .. color,
		groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		sounds = default.node_sound_wood_defaults(),
	})
end
--]]
end

local colors = {}
for _, entry in ipairs(mypaint_fence_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

