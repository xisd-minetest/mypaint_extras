-- Intllib.
local S = mypaint_default.gettext

local mypaint_sand_colors = mypaint_default.colors

local paintables = {
	"default:sand",
	"default:desert_sand",
	"default:silver_sand",
	"default:sandstone",
	"default:sandstonebrick",
	"default:sandstone_block"
}

for _, entry in ipairs(mypaint_sand_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"


-- Sand
local block_id = "default:sand"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_sand.png".. paint},
	groups = {crumbly = 3, falling_node = 1, sand = 1,cracky = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Desert Sand
local block_id = "default:desert_sand"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  S("Painted @1",minetest.registered_nodes[block_id].description),
	tiles = {"default_desert_sand.png".. paint},
	groups = {crumbly = 3, falling_node = 1, sand = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Silver Sand
local block_id = "default:silver_sand"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_silver_sand.png".. paint},
	groups = {crumbly = 3, falling_node = 1, sand = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Sandstone
local block_id = "default:sandstone"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_sandstone.png".. paint},
	groups = {crumbly = 2, cracky = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Sandstone Brick
local block_id = "default:sandstonebrick"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_sandstone_brick.png".. paint},
	groups = {crumbly = 2, cracky = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Sandstone Block
local block_id = "default:sandstone_block"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_sandstone_block.png".. paint},
	is_ground_content = false,
	groups = {cracky = 2,  not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})


end

local colors = {}
for _, entry in ipairs(mypaint_sand_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

