-- Intllib.
local S = mypaint_default.gettext

local mypaint_stone_colors = mypaint_default.colors

local paintables = {
	"default:cobble","default:desert_cobble","default:mossycobble",
	"default:stone","default:desert_stone","default:stone_block",
	"default:stonebrick","default:desert_stonebrick","default:desert_stone_block"
}

for _, entry in ipairs(mypaint_stone_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"
	local nici = entry[4]

-- Cobble
local block_id = "default:cobble"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_cobble.png".. paint},
	groups = {cracky = 3, stone = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})

-- Desert Cobble
local block_id = "default:desert_cobble"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_desert_cobble.png".. paint},
	groups = {cracky = 3, stone = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})

-- Mossy Cobble
local block_id = "default:mossycobble"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_mossycobble.png".. paint},
	groups = {cracky = 3, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})

-- Stone
local block_id = "default:stone"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_stone.png".. paint},
	groups = {cracky = 3, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Stone Block
local block_id = "default:stone_block"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_stone_block.png".. paint},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})

-- Desert Stone
local block_id = "default:desert_stone"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_desert_stone.png".. paint},
	groups = {cracky = 3, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Stone Brick
local block_id = "default:stonebrick"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_stone_brick.png".. paint},
	groups = {cracky = 2, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Desert Stone Brick
local block_id = "default:desert_stonebrick"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_desert_stone_brick.png".. paint},
	groups = {cracky = 2, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- Desert Stone Block
local block_id = "default:desert_stone_block"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_desert_stone_block.png".. paint},
	is_ground_content = false,
	groups = {cracky = 2, stone = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_stone_defaults(),
})


end

local colors = {}
for _, entry in ipairs(mypaint_stone_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

