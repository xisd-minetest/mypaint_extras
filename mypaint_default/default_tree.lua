-- Intllib.
local S = mypaint_default.gettext

local mypaint_tree_colors = mypaint_default.colors

local paintables = {
	"default:tree","default:acacia_tree","default:jungletree","default:aspen_tree","default:pine_tree",
}

for _, entry in ipairs(mypaint_tree_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"

-- Wood
local block_id = "default:tree"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_tree_top.png".. paint, "default_tree_top.png".. paint,
		"default_tree.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

---[[ Acacia
local block_id = "default:acacia_tree"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_acacia_tree_top.png".. paint, "default_acacia_tree_top.png".. paint,
		"default_acacia_tree.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Jungle
local block_id = "default:jungletree"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_jungletree_top.png".. paint, "default_jungletree_top.png".. paint,
		"default_jungletree.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Aspen
local block_id = "default:aspen_tree"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_aspen_tree_top.png".. paint, "default_aspen_tree_top.png".. paint,
		"default_aspen_tree.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Pine
local block_id = "default:pine_tree"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_pine_tree_top.png".. paint, "default_pine_tree_top.png".. paint,
		"default_pine_tree.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})


end

local colors = {}
for _, entry in ipairs(mypaint_tree_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

