-- Intllib.
local S = mypaint_default.gettext

local mypaint_wood_colors = mypaint_default.colors

local paintables = {
	"default:wood","default:acacia_wood","default:junglewood","default:aspen_wood","default:pine_wood",
}

for _, entry in ipairs(mypaint_wood_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"


-- Wood
local block_id = "default:wood"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_wood.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Acacia
local block_id = "default:acacia_wood"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_acacia_wood.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Jungle
local block_id = "default:junglewood"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_junglewood.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Aspen
local block_id = "default:aspen_wood"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_aspen_wood.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})

-- Pine
local block_id = "default:pine_wood"
minetest.register_node(':' .. block_id .. '_' .. color, {
	description =  minetest.registered_nodes[block_id].description,
	tiles = {"default_pine_wood.png".. paint},
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
	sounds = default.node_sound_wood_defaults(),
})



end

local colors = {}
for _, entry in ipairs(mypaint_wood_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

