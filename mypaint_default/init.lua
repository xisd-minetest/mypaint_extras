-- Load support for intllib.
local modpath = minetest.get_modpath(minetest.get_current_modname())
local S, NS = dofile(modpath.."/intllib.lua")

mypaint_default = {}
mypaint_default.gettext = S
mypaint_default.colors = {
		{"red",			"Red", 			"ff0000"},
 		{"green",		"Green", 		"00ff00"},
 		{"white",		"White", 		"ffffff"},
		{"black",		"Black",		"000000"},
		{"blue",		"Blue",			"0000ff"},
		{"brown",		"Brown",		"190B07"},
		{"cyan",		"Cyan",			"00ffff"},
		{"darkgreen",	"Dark Green",	"005000"},
		{"darkgrey",	"Dark Grey",	"1C1C1C"},
		{"grey",		"Grey",			"848484"},
		{"magenta",		"Magenta",		"ff00ff"},
		{"orange",		"Orange",		"ff7700"},
		{"pink",		"Pink",			"FE2E9A"},
		{"violet",		"Violet",		"7f007f"},
		{"yellow",		"Yellow",		"ffff00"},
}

dofile(modpath.."/default_dirt.lua")
dofile(modpath.."/default_gravel.lua")
dofile(modpath.."/default_sand.lua")
dofile(modpath.."/default_stone.lua")
dofile(modpath.."/default_tree.lua")
dofile(modpath.."/default_wood.lua")
dofile(modpath.."/default_fence.lua")
