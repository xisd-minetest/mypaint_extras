-- Load support for intllib.
local modpath = minetest.get_modpath(minetest.get_current_modname())
local S, NS = dofile(modpath.."/intllib.lua")

mypaint_stairs = {}
mypaint_stairs.gettext = S
mypaint_stairs.colors = {
		{"red",			"Red", 			"ff0000"},
 		{"green",		"Green", 		"00ff00"},
 		{"white",		"White", 		"ffffff"},
		{"black",		"Black",		"000000"},
		{"blue",		"Blue",			"0000ff"},
		{"brown",		"Brown",		"190B07"},
		{"cyan",		"Cyan",			"00ffff"},
		{"darkgreen",	"Dark Green",	"005000"},
		{"darkgrey",	"Dark Grey",	"1C1C1C"},
		{"grey",		"Grey",			"848484"},
		{"magenta",		"Magenta",		"ff00ff"},
		{"orange",		"Orange",		"ff7700"},
		{"pink",		"Pink",			"FE2E9A"},
		{"violet",		"Violet",		"7f007f"},
		{"yellow",		"Yellow",		"ffff00"},
}

local mypaint_stairs_colors = mypaint_stairs.colors

local paintables = {
	"stairs:stair_wood",
	"stairs:slab_wood",
	"stairs:stair_junglewood",
	"stairs:slab_junglewood",
	"stairs:stair_pine_wood",
	"stairs:slab_pine_wood",
	"stairs:stair_acacia_wood",
	"stairs:slab_acacia_wood",
	"stairs:stair_aspen_wood",
	"stairs:slab_aspen_wood",
	"stairs:stair_stone",
	"stairs:slab_stone",
	"stairs:stair_cobble",
	"stairs:slab_cobble",
	"stairs:stair_mossycobble",
	"stairs:slab_mossycobble",
	"stairs:stair_stonebrick",
	"stairs:slab_stonebrick",
	"stairs:stair_stone_block",
	"stairs:slab_stone_block",
	"stairs:stair_desert_stone",
	"stairs:slab_desert_stone",
	"stairs:stair_desert_cobble",
	"stairs:slab_desert_cobble",
	"stairs:stair_desert_stonebrick",
	"stairs:slab_desert_stonebrick",
	"stairs:stair_desert_stone_block",
	"stairs:stair_sandstone",
	"stairs:slab_sandstone",
	"stairs:stair_sandstonebrick",
	"stairs:slab_sandstonebrick",
	"stairs:stair_sandstone_block",
	"stairs:slab_sandstone_block",
}

for _, entry in ipairs(mypaint_stairs_colors) do
	local color = entry[1]
	local desc = entry[2]
	local paint = "^[colorize:#"..entry[3]..":100"


	stairs.register_stair_and_slab(
		"wood_" .. color,
		":default:wood_" .. color,
		{choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		{"default_wood.png"..paint},
		minetest.registered_nodes["stairs:stair_wood"].description,
		minetest.registered_nodes["stairs:slab_wood"].description,
		default.node_sound_wood_defaults()
	)

	stairs.register_stair_and_slab(
		"junglewood_" .. color,
		":default:junglewood_" .. color,
		{choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		{"default_junglewood.png"..paint},
		minetest.registered_nodes["stairs:stair_junglewood"].description,
		minetest.registered_nodes["stairs:slab_junglewood"].description,
		default.node_sound_wood_defaults()
	)

	stairs.register_stair_and_slab(
		"pine_wood_" .. color,
		":default:pine_wood_" .. color,
		{choppy = 3, oddly_breakable_by_hand = 2, flammable = 3, not_in_creative_inventory = 1},
		{"default_pine_wood.png"..paint},
		minetest.registered_nodes["stairs:stair_pine_wood"].description,
		minetest.registered_nodes["stairs:slab_pine_wood"].description,
		default.node_sound_wood_defaults()
	)

	stairs.register_stair_and_slab(
		"acacia_wood_" .. color,
		":default:acacia_wood_" .. color,
		{choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, not_in_creative_inventory = 1},
		{"default_acacia_wood.png"..paint},
		minetest.registered_nodes["stairs:stair_acacia_wood"].description,
		minetest.registered_nodes["stairs:slab_acacia_wood"].description,
		default.node_sound_wood_defaults()
	)

	stairs.register_stair_and_slab(
		"aspen_wood_" .. color,
		":default:aspen_wood_" .. color,
		{choppy = 3, oddly_breakable_by_hand = 2, flammable = 3, not_in_creative_inventory = 1},
		{"default_aspen_wood.png"..paint},
		minetest.registered_nodes["stairs:stair_aspen_wood"].description,
		minetest.registered_nodes["stairs:slab_aspen_wood"].description,
		default.node_sound_wood_defaults()
	)

	stairs.register_stair_and_slab(
		"stone_" .. color,
		":default:stone_" .. color,
		{cracky = 3, not_in_creative_inventory = 1},
		{"default_stone.png"..paint},
		minetest.registered_nodes["stairs:stair_stone"].description,
		minetest.registered_nodes["stairs:slab_stone"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"cobble_" .. color,
		":default:cobble_" .. color,
		{cracky = 3, not_in_creative_inventory = 1},
		{"default_cobble.png"..paint},
		minetest.registered_nodes["stairs:stair_cobble"].description,
		minetest.registered_nodes["stairs:slab_cobble"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"mossycobble_" .. color,
		nil,
		{cracky = 3, not_in_creative_inventory = 1},
		{"default_mossycobble.png"..paint},
		minetest.registered_nodes["stairs:stair_mossycobble"].description,
		minetest.registered_nodes["stairs:slab_mossycobble"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"stonebrick_" .. color,
		":default:stonebrick_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_stone_brick.png"..paint},
		minetest.registered_nodes["stairs:stair_stonebrick"].description,
		minetest.registered_nodes["stairs:slab_stonebrick"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"stone_block_" .. color,
		":default:stone_block_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_stone_block.png"..paint},
		minetest.registered_nodes["stairs:stair_stone_block"].description,
		minetest.registered_nodes["stairs:slab_stone_block"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"desert_stone_" .. color,
		":default:desert_stone_" .. color,
		{cracky = 3, not_in_creative_inventory = 1},
		{"default_desert_stone.png"..paint},
		minetest.registered_nodes["stairs:stair_desert_stone"].description,
		minetest.registered_nodes["stairs:slab_desert_stone"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"desert_cobble_" .. color,
		":default:desert_cobble_" .. color,
		{cracky = 3, not_in_creative_inventory = 1},
		{"default_desert_cobble.png"..paint},
		minetest.registered_nodes["stairs:stair_desert_cobble"].description,
		minetest.registered_nodes["stairs:slab_desert_cobble"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"desert_stonebrick_" .. color,
		":default:desert_stonebrick_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_desert_stone_brick.png"..paint},
		minetest.registered_nodes["stairs:stair_desert_stonebrick"].description,
		minetest.registered_nodes["stairs:slab_desert_stonebrick"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"desert_stone_block_" .. color,
		":default:desert_stone_block_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_desert_stone_block.png"..paint},
		minetest.registered_nodes["stairs:stair_desert_stone_block"].description,
		minetest.registered_nodes["stairs:slab_desert_stone_block"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"sandstone_" .. color,
		":default:sandstone_" .. color,
		{crumbly = 1, cracky = 3, not_in_creative_inventory = 1},
		{"default_sandstone.png"..paint},
		minetest.registered_nodes["stairs:stair_sandstone"].description,
		minetest.registered_nodes["stairs:slab_sandstone"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"sandstonebrick_" .. color,
		":default:sandstonebrick_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_sandstone_brick.png"..paint},
		minetest.registered_nodes["stairs:stair_sandstonebrick"].description,
		minetest.registered_nodes["stairs:slab_sandstonebrick"].description,
		default.node_sound_stone_defaults()
	)

	stairs.register_stair_and_slab(
		"sandstone_block_" .. color,
		":default:sandstone_block_" .. color,
		{cracky = 2, not_in_creative_inventory = 1},
		{"default_sandstone_block.png"..paint},
		minetest.registered_nodes["stairs:stair_sandstone_block"].description,
		minetest.registered_nodes["stairs:slab_sandstone_block"].description,
		default.node_sound_stone_defaults()
	)
	
end
	
local colors = {}
for _, entry in ipairs(mypaint_stairs_colors) do
	table.insert(colors, entry[1])
end
	mypaint.register(paintables, colors)

	
